# docker-texlive

Dockerfiles for [TeX Live](https://tug.org/texlive/).

## Available tags

- `full`: A full release containing all packages and tools from the installer.
- `minimal`: The infrastructure only, packages can be installed using `tlmgr`.

## How to use

Simply run `pdflatex` or another program (`latexmk` is available too):

```
docker run -v /my/.../directory:/var/tex itasks/texlive pdflatex doc.tex
```

## Source code

[gitlab.com/clean-and-itasks/docker-texlive](https://gitlab.com/clean-and-itasks/docker-texlive)

## Docker Hub

[hub.docker.com/r/itasks/texlive](https://hub.docker.com/r/itasks/texlive)

## Building new images

To build new images, the following steps should be performed:

```bash
docker build -t itasks/texlive:minimal minimal
docker build -t itasks/texlive:full full
```

Optionally, push these to the Docker Cloud:

```bash
docker push itasks/texlive:minimal
docker push itasks/texlive:full
```

